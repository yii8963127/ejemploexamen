<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "productos".
 *
 * @property int $idProducto
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $clasificacion
 * @property float|null $precio
 * @property string|null $foto
 *
 * @property Clasificacion $clasificacion0
 * @property Pedidos[] $pedidos
 */
class Productos extends \yii\db\ActiveRecord

{
    public $archivo;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */


    public function rules()
    {
        return [
            [['idProducto'], 'required'],
            [['idProducto', 'clasificacion'], 'integer'],
            [['precio'], 'number'],
            [['nombre', 'descripcion', 'foto'], 'string', 'max' => 100],
            // [['archivo'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg, png, jpeg'],
            [['archivo'], 'file',  'skipOnEmpty' => true, 'mimeTypes' => 'image/*'],

            [['nombre'], 'unique'],
            [['idProducto'], 'unique'],
            [['clasificacion'], 'exist', 'skipOnError' => true, 'targetClass' => Clasificacion::class, 'targetAttribute' => ['clasificacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idProducto' => 'Id Producto',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'clasificacion' => 'Clasificacion',
            'precio' => 'Precio',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[Clasificacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClasificacion0()
    {
        return $this->hasOne(Clasificacion::class, ['id' => 'clasificacion']);
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::class, ['idProducto' => 'idProducto']);
    }
    public function beforeValidate(): bool
    {
        // Comprobamos si has seleccionado una foto 
        if (isset($this->archivo)) {
            $this->archivo = UploadedFile::getInstance($this, "archivo");
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }

    public function afterValidate(): bool
    {
        // Volvemos a comprobar si se ha subido una foto
        if (isset($this->archivo)) {
            $this->subirArchivo();
            $this->foto = $this->idProducto . '_' . $this->archivo->name; // Al campo foto le guardamos el nombre de la foto
        }
        return true; // este metodo te pide que devuelvas true. Si no devuelves true no hace nada
    }
    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/productos/' . $this->idProducto . '_' . $this->archivo->name, false);  // false, si la foto existe, no sube la nueva para no destruir la que está
        return true;
    }

    // Disparador que se ejecuta después de eliminar un registro
    public function afterDelete()
    {
        // Elimino la imagen de la noticia de web/imgs
        if (isset($this->foto) && file_exists(Yii::getAlias("@webroot") . '/imgs/productos/' . $this->foto)) {
            unlink('imgs/productos/' . $this->foto);
        }
        return true;
    }


    /**
     * afterSave 
     * Este método se ejecuta después de guardar el registro en la BBDD
     * 
     * @param  mixed $insert este argumento es true si se está insertando un registro y false si es una actualización
     * @param  array $atributosAnteriores Array con todos los datos de la tabla antes de actualizar
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {
        // Si estoy actualizando datos
        if (!$insert) {
            // Si la noticia tenia ya una foto y hemos adjuntado una nueva, elimina la vieja
            if (isset($this->archivo) && isset($atributosAnteriores['foto'])) {
                unlink('imgs/productos/' . $atributosAnteriores['foto']);
            }
        }
    }
    public function getClasificaciones()
    {
        $clasificacion = Clasificacion::find()->all();
        return ArrayHelper::map($clasificacion, 'id', 'nombre');
    }

    public function getPrecioeuros()
    {
        return isset($this->precio) ? $this->precio . ' €' : '';
    }
}
