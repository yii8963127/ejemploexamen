<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pedidos".
 *
 * @property int $idPedido
 * @property int $idCliente
 * @property int $idProducto
 * @property int $cantidad
 * @property string|null $observacion
 * @property string|null $fechaHora
 *
 * @property Clientes $idCliente0
 * @property Productos $idProducto0
 */
class Pedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPedido', 'idCliente', 'idProducto', 'cantidad'], 'required'],
            [['idPedido', 'idCliente', 'idProducto', 'cantidad'], 'integer'],
            [['fechaHora'], 'date', 'format' => 'yyyy-MM-dd'],
            [['observacion'], 'string', 'max' => 100],
            [['idPedido'], 'unique'],
            [['idCliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::class, 'targetAttribute' => ['idCliente' => 'idCliente']],
            [['idProducto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProducto' => 'idProducto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idPedido' => 'Id Pedido',
            'idCliente' => 'Cliente',
            'idProducto' => 'Producto',
            'cantidad' => 'Cantidad',
            'observacion' => 'Observacion',
            'fechaHora' => 'Fecha Hora',
        ];
    }

    /**
     * Gets query for [[IdCliente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCliente0()
    {
        return $this->hasOne(Clientes::class, ['idCliente' => 'idCliente']);
    }

    /**
     * Gets query for [[IdProducto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto0()
    {
        return $this->hasOne(Productos::class, ['idProducto' => 'idProducto']);
    }

    public function getProductos()
    {
        $productos = Productos::find()->all();
        return ArrayHelper::map($productos, 'idProducto', 'nombre');
    }

    public function getClientes()
    {
        $clientes = Clientes::find()->all();
        return ArrayHelper::map($clientes, 'idCliente', 'nombre');
    }
}
