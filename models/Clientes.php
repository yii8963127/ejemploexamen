<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $idCliente
 * @property string|null $nombre
 * @property string|null $direccion
 * @property string|null $poblacion
 * @property string|null $codigoPostal
 * @property int|null $telefono
 *
 * @property Pedidos[] $pedidos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCliente'], 'required'],
            [['idCliente', 'telefono'], 'integer'],
            [['nombre', 'direccion', 'poblacion'], 'string', 'max' => 100],
            [['codigoPostal'], 'string', 'max' => 20],
            [['idCliente'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCliente' => 'Id Cliente',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'poblacion' => 'Poblacion',
            'codigoPostal' => 'Codigo Postal',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[Pedidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedidos::class, ['idCliente' => 'idCliente']);
    }
}
