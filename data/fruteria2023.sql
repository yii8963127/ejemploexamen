﻿DROP DATABASE IF EXISTS fruteria2023;
CREATE DATABASE fruteria2023;
USE fruteria2023;


CREATE TABLE productos(
  idProducto INT NOT NULL,
  nombre VARCHAR(100) DEFAULT NULL,
descripcion varchar(100),  
clasificacion INT,
precio float, 
foto varchar(100),
  PRIMARY KEY (idProducto),
  UNIQUE KEY (nombre)
  );



CREATE TABLE clientes(
  idCliente INT NOT NULL,
  nombre VARCHAR(100) DEFAULT NULL,
  direccion varchar(100),
  poblacion varchar(100),
codigoPostal varchar(20),
telefono int,
  PRIMARY KEY (idCliente)


  );

CREATE TABLE pedidos(
  idPedido INT NOT NULL,
  idCliente int not null,
  idProducto INT NOT NULL,
  cantidad int not null,
  observacion varchar(100) DEFAULT NULL,
  fechaHora datetime,
  PRIMARY KEY (idPedido)


  );

CREATE TABLE clasificacion (
id int,
nombre varchar(100),
PRIMARY KEY (id)
);

INSERT INTO  productos (idProducto, nombre, clasificacion, precio)   
VALUES              
(80, 'fresa', 1, 10),
(81, 'banana',  1, 2),
(82, 'naranja', 1, 3),
(50, 'Lombarda', 2, 5),
(51, 'cebolla', 2, 3),
(52, 'puerro', 2, 1);





ALTER TABLE pedidos
ADD CONSTRAINT fkPedidoCliente FOREIGN KEY(idCliente) REFERENCES clientes(idCliente),
ADD CONSTRAINT fkPedidoProducto FOREIGN KEY(idProducto) REFERENCES productos(idProducto);


ALTER TABLE productos
ADD CONSTRAINT fkProductosClasificacion FOREIGN KEY (clasificacion) REFERENCES clasificacion(id);




