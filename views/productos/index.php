<?php

use app\models\Productos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idProducto',
            'nombre',
            'descripcion',
            //'clasificacion',
            [
                'attribute' => 'clasificacion',
                'value' => function ($model) {
                    return $model->clasificacion0->nombre;
                }
            ],
            //'precio',
            [
                'attribute' => 'precio',
                'value' => function ($model) {
                    return $model->precioeuros;
                }
            ],
            //'foto',
            [
                'label' => 'Foto',
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->foto)) {
                        return Html::img("@web/imgs/productos/{$dato->foto}", ["width" => 200, "height" => 150]);
                    } else {
                        return Html::img('@web/imgs/notfound.png', ['width' => 200, 'height' => 150]);
                    }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Productos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idProducto' => $model->idProducto]);
                }
            ],
        ],
    ]); ?>


</div>