<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Productos $model */

$this->title = $model->idProducto;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'idProducto' => $model->idProducto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'idProducto' => $model->idProducto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProducto',
            'nombre',
            'descripcion',
            //'clasificacion',
            [
                'attribute' => 'clasificacion',
                'value' => function ($model) {
                    return $model->clasificacion0->nombre;
                }
            ],
            //'precio',
            [
                'attribute' => 'precio',
                'value' => function ($model) {
                    return $model->precioeuros;
                }
            ],
            //'foto',
            [
                'label' => 'Foto',
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => function ($dato) {
                    if (isset($dato->foto)) {
                        return Html::img("@web/imgs/productos/{$dato->foto}", ["width" => 200, "height" => 150]);
                    } else {
                        return Html::img('@web/imgs/notfound.png', ['width' => 200, 'height' => 150]);
                    }
                }
            ],
        ],
    ]) ?>

</div>