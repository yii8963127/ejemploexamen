<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedidos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedidos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPedido')->input('number') ?>

    <?= $form->field($model, 'idCliente')->dropDownList($model->clientes, ['prompt' => 'Selecciona el cliente']) ?>

    <?= $form->field($model, 'idProducto')->dropDownList($model->productos, ['prompt' => 'Selecciona el producto'])  ?>

    <?= $form->field($model, 'cantidad')->input('number') ?>

    <?= $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaHora')->input('date') ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>