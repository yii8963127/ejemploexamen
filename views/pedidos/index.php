<?php

use app\models\Pedidos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Pedidos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedidos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Pedidos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idPedido',
            [
                'attribute' => 'idCliente',
                'value' => function ($model) {
                    return $model->idCliente0->nombre;
                }
            ],

            //'idCliente',
            [
                'attribute' => 'idProducto',
                'value' => function ($model) {
                    return $model->idProducto0->nombre;
                }
            ],
            //'idProducto',
            'cantidad',
            'observacion',
            'fechaHora',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Pedidos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idPedido' => $model->idPedido]);
                }
            ],
        ],
    ]); ?>


</div>