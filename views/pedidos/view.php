<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Pedidos $model */

$this->title = $model->idPedido;
$this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pedidos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idPedido' => $model->idPedido], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idPedido' => $model->idPedido], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPedido',
            [
                'attribute' => 'idCliente',
                'value' => function ($model) {
                    return $model->idCliente0->nombre;
                }
            ],

            //'idCliente',
            [
                'attribute' => 'idProducto',
                'value' => function ($model) {
                    return $model->idProducto0->nombre;
                }
            ],
            //'idProducto',
            'cantidad',
            'observacion',
            'fechaHora',
        ],
    ]) ?>

</div>