<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Bienvenidos!</h1>

        <div>
            <?= Html::a('Hacer pedido', ['pedidos/create'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <div class="contenedor">

        <div>
            <?= Html::img(
                '@web/imgs/grocery.jpg',
                // ruta a la imagen 
                [
                    'class' => '',
                    'style' => '',  // o puedes poner 'width:500px'
                    'width' => '300px',
                    'height' => '300px',
                ],
            ) ?>
        </div>
        <div>
            <?= Html::img(
                '@web/imgs/frutas.jpg',
                // ruta a la imagen 
                [
                    'class' => '',
                    'style' => '',  // o puedes poner 'width:500px'
                    'width' => '300px',
                    'height' => '300px',
                ],
            ) ?>
        </div>
        <div>
            <?= Html::img(
                '@web/imgs/tomatos.jpg',
                // ruta a la imagen 
                [
                    'class' => '',
                    'style' => '',  // o puedes poner 'width:500px'
                    'width' => '300px',
                    'height' => '300px',
                ],
            ) ?>
        </div>
    </div>

</div>