<?php

use yii\helpers\Html;

?>
<div class="row">
    <?php
    foreach ($clasificaciones as $clasificacion) {
    ?>
        <div class="col-6 mb-3">
            <div class="card bg-primary text-white">
                <div class="card-body carta">
                    <h3 class="card-title"><?= $clasificacion->nombre ?></h3>
                    <p><?= Html::a('Ver sección', ['site/productos', 'id' => $clasificacion->id], ['class' => 'btn btn-light']) ?></p>

                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>