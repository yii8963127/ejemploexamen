<?php

use yii\helpers\Html;
?>

<h2 style="text-align: center;margin:25px"> <?= $dato->nombre ?></h2>
<table class="tablaProducto">
    <tr>
        <td>Nombre producto</td>
        <td><?= $dato->nombre ?></td>
    </tr>
    <tr>
        <td>Descripción</td>
        <td><?= $dato->descripcion ?></td>
    </tr>
    <tr>
        <td>Foto</td>
        <td><?= Html::img("@web/imgs/productos/{$dato->foto}", ['width' => '400px', 'class' => 'img-fluid']) ?></td>
    </tr>
    <tr>
        <td>Precio</td>
        <td> <?= $dato->precio ?> €</td>
    </tr>
    <tr>
        <td>Sección</td>
        <td> <?= $dato->clasificacion0->nombre ?></td>
    </tr>

</table>