<?php

use yii\helpers\Html;

?>

<div class="producto">

    <h3><?= $dato['nombre'] ?></h3>


    <p><?= $dato['precio'] ?> €</p>

    <p>
        <?php
        if (isset($dato['foto'])) {
            echo Html::img("@web/imgs/productos/{$dato['foto']}", ['style' => 'width:200px;height: 200px', 'class' => 'img-thumbnail']);
        } else {
            echo  Html::img("@web/imgs/notfound.png", ['style' => 'width:200px;height: 200px', 'class' => 'img-thumbnail']);
        }
        ?>
    </p>

    <?= Html::a('Más detalles', ['site/verproducto', 'idProducto' => $dato->idProducto], ['class' => 'botonDetalles']) ?>

</div>